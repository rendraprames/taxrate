package com.rendra.test.dto;

import com.rendra.test.entity.Employee;
import com.rendra.test.entity.SalaryComponent;
import jakarta.persistence.Entity;
import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SalaryRequest {
    private Employee employee;
    private SalaryComponent[] salaryComponents;
}
