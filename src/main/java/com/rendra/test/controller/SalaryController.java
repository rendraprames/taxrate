package com.rendra.test.controller;

import com.rendra.test.dto.SalaryRequest;
import com.rendra.test.dto.TaxResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;

public interface SalaryController {

    @PostMapping("/hitungpajak")
    ResponseEntity<TaxResponse> calculateSalaryTax(@RequestBody SalaryRequest request);
}
