package com.rendra.test.controller.impl;

import com.rendra.test.controller.SalaryController;
import com.rendra.test.dto.SalaryRequest;
import com.rendra.test.dto.TaxResponse;
import com.rendra.test.entity.Employee;
import com.rendra.test.entity.SalaryComponent;
import com.rendra.test.service.impl.TaxCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class SalaryControllerImpl implements SalaryController {

    @Autowired
    private TaxCalculatorService taxCalculatorService;

    public ResponseEntity<TaxResponse> calculateSalaryTax(@RequestBody SalaryRequest request) {
        Employee employee = request.getEmployee();
        SalaryComponent[] salaryComponents = request.getSalaryComponents();

        TaxResponse tax = taxCalculatorService.calculateTax(employee, salaryComponents);

        return new ResponseEntity<>(tax, HttpStatus.OK);
    }

}
