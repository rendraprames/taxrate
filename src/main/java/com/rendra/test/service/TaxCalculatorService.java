package com.rendra.test.service;

import com.rendra.test.dto.TaxResponse;
import com.rendra.test.entity.Employee;
import com.rendra.test.entity.SalaryComponent;

import java.math.BigDecimal;

public interface TaxCalculatorService {
    TaxResponse calculateTax(Employee employee, SalaryComponent[] salaryComponents);
}
