package com.rendra.test.service.impl;

import com.rendra.test.dto.TaxData;
import com.rendra.test.dto.TaxResponse;
import com.rendra.test.dto.TaxResult;
import com.rendra.test.entity.Employee;
import com.rendra.test.entity.SalaryComponent;
import com.rendra.test.service.TaxCalculatorService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;

@Service
public class VietnamTaxCalculatorServiceImpl implements TaxCalculatorService {
    private static final BigDecimal BELUM_KAWIN_PTKP = new BigDecimal("15");
    private static final BigDecimal SUDAH_KAWIN_PTKP = new BigDecimal("30");
    private static final BigDecimal TAX_RATE_1 = new BigDecimal("0.025");
    private static final BigDecimal TAX_RATE_2 = new BigDecimal("0.075");

    @Override
    public TaxResponse calculateTax(Employee employee, SalaryComponent[] salaryComponents) {
        BigDecimal totalIncome = calculateTotalIncome(salaryComponents);
        BigDecimal ptkp = calculatePTKP(employee);

        BigDecimal netIncome = totalIncome.subtract(ptkp);
        BigDecimal tax = BigDecimal.ZERO;

        TaxResponse taxResponse = new TaxResponse();

        if (netIncome.compareTo(BigDecimal.ZERO) > 0) {
            if (netIncome.compareTo(new BigDecimal("50")) <= 0) {
                tax = netIncome.multiply(TAX_RATE_1);
            } else {
                BigDecimal taxableIncome = netIncome.subtract(new BigDecimal("50"));
                tax = taxableIncome.multiply(TAX_RATE_2).add(new BigDecimal("1.25"));
            }
        }

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        String formattedTax = decimalFormat.format(tax);

        TaxResult taxResult = new TaxResult(formattedTax, formattedTax);
        TaxData taxData = new TaxData(taxResult);

        taxResponse.setStatus("200");
        taxResponse.setMessage("success");
        taxResponse.setData(taxData);

        return taxResponse;
    }

    private BigDecimal calculateTotalIncome(SalaryComponent[] salaryComponents) {
        BigDecimal totalIncome = BigDecimal.ZERO;

        for (SalaryComponent component : salaryComponents) {
            if ("earning".equals(component.getType())) {
                totalIncome = totalIncome.add(component.getAmount());
            }
        }

        return totalIncome;
    }

    private BigDecimal calculatePTKP(Employee employee) {
        if ("belum kawin".equalsIgnoreCase(employee.getMaritalStatus())) {
            return BELUM_KAWIN_PTKP;
        } else if ("sudah kawin".equalsIgnoreCase(employee.getMaritalStatus())) {
            return SUDAH_KAWIN_PTKP;
        }

        return BigDecimal.ZERO;
    }
}
