package com.rendra.test.service.impl;

import com.rendra.test.dto.TaxResponse;
import com.rendra.test.entity.Employee;
import com.rendra.test.entity.SalaryComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class TaxCalculatorService {
    @Autowired
    private final IndonesiaTaxCalculatorServiceImpl indonesiaTaxCalculatorService;

    @Autowired
    private final VietnamTaxCalculatorServiceImpl vietnamTaxCalculatorService;

    public TaxCalculatorService(IndonesiaTaxCalculatorServiceImpl indonesiaTaxCalculatorService, VietnamTaxCalculatorServiceImpl vietnamTaxCalculatorService) {
        this.indonesiaTaxCalculatorService = indonesiaTaxCalculatorService;
        this.vietnamTaxCalculatorService = vietnamTaxCalculatorService;
    }

    public TaxResponse calculateTax(Employee employee, SalaryComponent[] salaryComponents) {
        String country = employee.getCountry();
        if ("indonesia".equalsIgnoreCase(country)) {
            return indonesiaTaxCalculatorService.calculateTax(employee, salaryComponents);
        } else if ("vietnam".equalsIgnoreCase(country)) {
            return vietnamTaxCalculatorService.calculateTax(employee, salaryComponents);
        } else {
            throw new IllegalArgumentException("Unsupported country: " + country);
        }
    }
}
