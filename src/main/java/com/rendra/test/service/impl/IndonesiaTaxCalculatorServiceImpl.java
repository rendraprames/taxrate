package com.rendra.test.service.impl;

import com.rendra.test.dto.TaxData;
import com.rendra.test.dto.TaxResponse;
import com.rendra.test.dto.TaxResult;
import com.rendra.test.entity.Employee;
import com.rendra.test.entity.SalaryComponent;
import com.rendra.test.service.TaxCalculatorService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

@Service
public class IndonesiaTaxCalculatorServiceImpl implements TaxCalculatorService {
    private static final BigDecimal PTKP_TK = new BigDecimal("25000000");
    private static final BigDecimal PTKP_K0 = new BigDecimal("50000000");
    private static final BigDecimal PTKP_K1 = new BigDecimal("75000000");
    private static final BigDecimal TAX_RATE_1 = new BigDecimal("0.05");
    private static final BigDecimal TAX_RATE_2 = new BigDecimal("0.1");
    private static final BigDecimal TAX_RATE_3 = new BigDecimal("0.15");

    @Override
    public TaxResponse calculateTax(Employee employee, SalaryComponent[] salaryComponents) {
        BigDecimal annualSalary = calculateAnnualSalary(salaryComponents);
        BigDecimal netIncome = annualSalary.subtract(getPTKP(employee));
        BigDecimal annualTax = calculateAnnualTax(netIncome);

        BigDecimal monthlyTax = annualTax.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP);
        BigDecimal yearlyTax = annualTax.setScale(0, RoundingMode.HALF_UP);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        String formattedMonthlyTax = decimalFormat.format(monthlyTax);
        String formattedYearlyTax = decimalFormat.format(yearlyTax);

        TaxResult taxResult = new TaxResult(formattedMonthlyTax, formattedYearlyTax);
        TaxData taxData = new TaxData(taxResult);
        return new TaxResponse("200", "success", taxData);
    }

    private BigDecimal calculateAnnualSalary(SalaryComponent[] salaryComponents) {
        BigDecimal monthlySalary = BigDecimal.ZERO;
        for (SalaryComponent component : salaryComponents) {
            if ("earning".equalsIgnoreCase(component.getType())) {
                monthlySalary = component.getAmount();
                break;
            }
        }
        return monthlySalary.multiply(BigDecimal.valueOf(12));
    }

    private BigDecimal getPTKP(Employee employee) {
        String maritalStatus = employee.getMaritalStatus();

        if ("Belum kawin".equalsIgnoreCase(maritalStatus)) {
            return PTKP_TK;
        } else if ("Kawin dan belum punya anak".equalsIgnoreCase(maritalStatus)) {
            return PTKP_K0;
        } else if ("Kawin dan sudah punya anak".equalsIgnoreCase(maritalStatus)) {
            return PTKP_K1;
        } else {
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal calculateAnnualTax(BigDecimal annualNetIncome) {
        BigDecimal layer1Income = BigDecimal.valueOf(50000000);
        BigDecimal layer1TaxRate = BigDecimal.valueOf(0.05);
        BigDecimal layer1Tax;

        if (annualNetIncome.compareTo(layer1Income) > 0) {
            layer1Tax = layer1Income.multiply(layer1TaxRate);
            annualNetIncome = annualNetIncome.subtract(layer1Income);
        } else {
            return annualNetIncome.multiply(layer1TaxRate);
        }

        BigDecimal layer2Income = annualNetIncome;
        BigDecimal layer2TaxRate = BigDecimal.ZERO;

        if (layer2Income.compareTo(BigDecimal.valueOf(50000000)) > 0 && layer2Income.compareTo(BigDecimal.valueOf(250000000)) <= 0) {
            layer2TaxRate = BigDecimal.valueOf(0.1);
        } else if (layer2Income.compareTo(BigDecimal.valueOf(250000000)) > 0) {
            layer2TaxRate = BigDecimal.valueOf(0.15);
        }

        BigDecimal layer2Tax = layer2Income.multiply(layer2TaxRate);

        BigDecimal annualTax = layer1Tax.add(layer2Tax);
        return annualTax;
    }
}
