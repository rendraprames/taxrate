package com.rendra.test.entity;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Employee {
    private String name;
    private String sex;
    private String maritalStatus;
    private int child;
    private String country;
}
