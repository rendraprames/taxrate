package com.rendra.test.entity;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SalaryComponent {
    private String name;
    private String type;
    private BigDecimal amount;
}
